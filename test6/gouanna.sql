--------------------------------------------------------
--  文件已创建 - 星期五-五月-26-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ANNA_CUSTOMER
--------------------------------------------------------

  CREATE TABLE "ANNA"."ANNA_CUSTOMER" 
   (	"ID" NUMBER(10,0), 
	"NAME" VARCHAR2(50 BYTE), 
	"PHONE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_DATA" ;
--------------------------------------------------------
--  DDL for Table ANNA_LOG
--------------------------------------------------------

  CREATE TABLE "ANNA"."ANNA_LOG" 
   (	"ID" NUMBER(10,0), 
	"TRANSACTION_TIME" TIMESTAMP (6), 
	"AMOUNT" NUMBER(10,2), 
	"CUSTOMER_ID" NUMBER(10,0), 
	"PRODUCT_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_INDEX" ;
--------------------------------------------------------
--  DDL for Table ANNA_ORDER
--------------------------------------------------------

  CREATE TABLE "ANNA"."ANNA_ORDER" 
   (	"ID" NUMBER(10,0), 
	"CUSTOMER_ID" NUMBER(10,0), 
	"PRODUCT_ID" NUMBER(10,0), 
	"TOTAL_PRICE" NUMBER(10,2), 
	"ORDER_TIME" TIMESTAMP (6)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_DATA" ;
--------------------------------------------------------
--  DDL for Table ANNA_PRODUCT
--------------------------------------------------------

  CREATE TABLE "ANNA"."ANNA_PRODUCT" 
   (	"ID" NUMBER(10,0), 
	"NAME" VARCHAR2(50 BYTE), 
	"PRICE" NUMBER(10,2), 
	"STOCK" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_DATA" ;
--------------------------------------------------------
--  DDL for Table ANNA_STOCK
--------------------------------------------------------

  CREATE TABLE "ANNA"."ANNA_STOCK" 
   (	"PRODUCT_ID" NUMBER(10,0), 
	"STOCK" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_INDEX" ;
--------------------------------------------------------
--  DDL for Table CUSTOMERS
--------------------------------------------------------

  CREATE TABLE "ANNA"."CUSTOMERS" 
   (	"ID" NUMBER(10,0), 
	"NAME" VARCHAR2(50 BYTE), 
	"PHONE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_DATA" ;
--------------------------------------------------------
--  DDL for Table PRODUCT
--------------------------------------------------------

  CREATE TABLE "ANNA"."PRODUCT" 
   (	"ID" NUMBER(10,0), 
	"NAME" VARCHAR2(50 BYTE), 
	"PRICE" NUMBER(10,2), 
	"STOCK" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_DATA" ;
--------------------------------------------------------
--  DDL for Table SALES_LOG
--------------------------------------------------------

  CREATE TABLE "ANNA"."SALES_LOG" 
   (	"ID" NUMBER(10,0), 
	"TRANSACTION_TIME" TIMESTAMP (6), 
	"AMOUNT" NUMBER(10,2), 
	"CUSTOMER_ID" NUMBER(10,0), 
	"PRODUCT_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_INDEX" ;
--------------------------------------------------------
--  DDL for Table SALES_ORDERA
--------------------------------------------------------

  CREATE TABLE "ANNA"."SALES_ORDERA" 
   (	"ID" NUMBER(10,0), 
	"CUSTOMER_ID" NUMBER(10,0), 
	"PRODUCT_ID" NUMBER(10,0), 
	"TOTAL_PRICE" NUMBER(10,2), 
	"ORDER_TIME" TIMESTAMP (6)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_DATA" ;
--------------------------------------------------------
--  DDL for Table SALES_ORDERS
--------------------------------------------------------

  CREATE TABLE "ANNA"."SALES_ORDERS" 
   (	"ID" NUMBER(10,0), 
	"CUSTOMER_ID" NUMBER(10,0), 
	"PRODUCT_ID" NUMBER(10,0), 
	"TOTAL_PRICE" NUMBER(10,2), 
	"ORDER_TIME" TIMESTAMP (6)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_DATA" ;
--------------------------------------------------------
--  DDL for Table STOCK
--------------------------------------------------------

  CREATE TABLE "ANNA"."STOCK" 
   (	"PRODUCT_ID" NUMBER(10,0), 
	"STOCK" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ANNA_INDEX" ;
REM INSERTING into ANNA.ANNA_CUSTOMER
SET DEFINE OFF;
REM INSERTING into ANNA.ANNA_LOG
SET DEFINE OFF;
REM INSERTING into ANNA.ANNA_ORDER
SET DEFINE OFF;
REM INSERTING into ANNA.ANNA_PRODUCT
SET DEFINE OFF;
REM INSERTING into ANNA.ANNA_STOCK
SET DEFINE OFF;
REM INSERTING into ANNA.CUSTOMERS
SET DEFINE OFF;
REM INSERTING into ANNA.PRODUCT
SET DEFINE OFF;
REM INSERTING into ANNA.SALES_LOG
SET DEFINE OFF;
REM INSERTING into ANNA.SALES_ORDERA
SET DEFINE OFF;
REM INSERTING into ANNA.SALES_ORDERS
SET DEFINE OFF;
REM INSERTING into ANNA.STOCK
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index CUSTOMER_ID_INDEX
--------------------------------------------------------

  CREATE INDEX "ANNA"."CUSTOMER_ID_INDEX" ON "ANNA"."ANNA_CUSTOMER" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "ANNA_INDEX" ;
--------------------------------------------------------
--  DDL for Index PRODUCT_ID_INDEX
--------------------------------------------------------

  CREATE INDEX "ANNA"."PRODUCT_ID_INDEX" ON "ANNA"."ANNA_PRODUCT" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "ANNA_INDEX" ;
--------------------------------------------------------
--  DDL for Index CUSTOMER_PRODUCT_INDEX
--------------------------------------------------------

  CREATE INDEX "ANNA"."CUSTOMER_PRODUCT_INDEX" ON "ANNA"."ANNA_ORDER" ("CUSTOMER_ID", "PRODUCT_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "ANNA_INDEX" ;
