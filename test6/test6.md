# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010414304    姓名：苟安娜

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 创建用户并授权,连接数据库

``` sql
create user gouanna identified by 123456;
grant connect, resource,dba to gouanna;
grant unlimited tablespace  to gouanna;
```

![pic1](E:\orace作业\test6\pic1.png)

![pic2](E:\orace作业\test6\pic2.png)

#### 创建表空间

```
CREATE TABLESPACE anna_data_ts 
DATAFILE '/home/oracle/app/oracle/oradata/orcl/anna_data_ts.dbf' 
SIZE 500M 
AUTOEXTEND ON 
EXTENT MANAGEMENT LOCAL 
SEGMENT SPACE MANAGEMENT AUTO;


CREATE TABLESPACE index_ts
DATAFILE '/home/oracle/app/oracle/oradata/orcl/index_ts.dbf' 
SIZE 1000M 
AUTOEXTEND ON 
EXTENT MANAGEMENT LOCAL 
SEGMENT SPACE MANAGEMENT AUTO;
```

![pic3](E:\orace作业\test6\pic3.png)

##### 表空间结构

| 表空间名称    | 表名称     | 表描述               |
| ------------- | :--------- | -------------------- |
| anna_data_ts  | products   | 商品信息表           |
| anna_data_ts  | own_orders | 订单信息表           |
| anna_data_ts  | customer   | 客户信息表           |
| anna_data_ts  | cart       | 购物车信息表         |
| anna_data_ts  | payment    | 支付记录信息表       |
| index_temp_ts |            | 存储各个表的相关索引 |

anna_data_ts表空间是用于存储数据表的数据文件，此表空间为大容量、高速读取的表空间。其中包括客户信息表、订单表等。

index_temp_ts表空间是用于存储索引表，此表空间为高速写入、高速读取的表空间。其中包括客户信息表和订单表的索引表。

#### 创建相关表

##### 1.商品信息表

```sql
-- 创建商品信息表
CREATE TABLE product (
    id NUMBER(10) PRIMARY KEY,
    name VARCHAR2(50) NOT NULL,
    price NUMBER(10,2) NOT NULL,
    description VARCHAR2(500),
    stock NUMBER(10) DEFAULT 0 NOT NULL
) TABLESPACE anna_data_ts;
```

![pic4](E:\orace作业\test6\pic4.png)



##### 2.订单信息表

```sql
-- 创建订单信息表
CREATE TABLE own_order (
    id NUMBER(10) PRIMARY KEY,
    time DATE NOT NULL,
    total NUMBER(10,2) NOT NULL,
    status VARCHAR2(20) NOT NULL,
    customer_id NUMBER(10) NOT NULL,
    CONSTRAINT fk_order_customer
        FOREIGN KEY (customer_id)
        REFERENCES customer(id)
) TABLESPACE anna_data_ts;
```

![pic5](E:\orace作业\test6\pic5.png)

##### 3.客户信息表

```sql
-- 创建客户信息表
CREATE TABLE customer (
    id NUMBER(10) PRIMARY KEY,
    name VARCHAR2(50) NOT NULL,
    phone VARCHAR2(15) NOT NULL,
    address VARCHAR2(100) NOT NULL
) TABLESPACE anna_data_ts;
```

![pic6](E:\orace作业\test6\pic6.png)

##### 4.购物车信息表

```sql
-- 创建购物车信息表
CREATE TABLE cart (
    id NUMBER(10) PRIMARY KEY,
    customer_id NUMBER(10) NOT NULL,
    product_id NUMBER(10) NOT NULL,
    quantity NUMBER(10) DEFAULT 1 NOT NULL,
    CONSTRAINT fk_cart_customer
        FOREIGN KEY (customer_id)
        REFERENCES customer(id),
    CONSTRAINT fk_cart_product
        FOREIGN KEY (product_id)
        REFERENCES product(id)
) TABLESPACE anna_data_ts;
```

![pic7](E:\orace作业\test6\pic7.png)

##### 5.支付记录表

```sql
-- 创建支付记录表
CREATE TABLE payment (
    id NUMBER(10) PRIMARY KEY,
    order_id NUMBER(10) NOT NULL,
    time DATE NOT NULL,
    method VARCHAR2(20) NOT NULL,
    amount NUMBER(10,2) NOT NULL,
    CONSTRAINT fk_payment_order
        FOREIGN KEY (order_id)
        REFERENCES own_order(id)
) TABLESPACE anna_data_ts;
```

![pic8](E:\orace作业\test6\pic8.png)



#### 插入填充数据

##### 1.填充商品信息表

```sql
-- 生成30,000个商品ID
DECLARE
    i NUMBER(10);
BEGIN
    FOR i IN 1..30000 LOOP
        INSERT INTO product (id, name, price, description, stock)
        VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2), 'This is product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 100)));
    END LOOP;
    COMMIT;
END;
```

![pic9](E:\orace作业\test6\pic9.png)

##### 2.填充客户信息表

```sql
-- 生成30,000个客户ID
DECLARE
    i NUMBER(10);
BEGIN
    FOR i IN 1..30000 LOOP
        INSERT INTO customer (id, name, phone, address)
        VALUES (i, 'Customer ' || i, '(' || TO_CHAR(ROUND(DBMS_RANDOM.VALUE(1300000, 1399999))) || ')' || TO_CHAR(ROUND(DBMS_RANDOM.VALUE(10000000, 99999999))), 'Address ' || i);
    END LOOP;
    COMMIT;
END;
```

![pic10](E:\orace作业\test6\pic10.png)

##### 3.填充购物车信息表

```sql
-- 生成30,000个购物车记录
DECLARE
    i NUMBER(10);
    customer_id NUMBER(10);
    product_id NUMBER(10);
BEGIN
    FOR i IN 1..30000 LOOP
        SELECT id INTO customer_id FROM customer WHERE ROWNUM = 1 ORDER BY DBMS_RANDOM.VALUE;
        SELECT id INTO product_id FROM product WHERE ROWNUM = 1 ORDER BY DBMS_RANDOM.VALUE;
        INSERT INTO cart (id, customer_id, product_id, quantity)
        VALUES (i, customer_id, product_id, ROUND(DBMS_RANDOM.VALUE(1, 10)));
    END LOOP;
    COMMIT;
END;
```

![pic11](E:\orace作业\test6\pic11.png)



#### 设计权限及用户分配

##### 创建管理员用户

```sql
CREATE USER admin1 IDENTIFIED BY 123456;
```

![pic12](E:\orace作业\test6\pic12.png)

##### 为管理员赋予权限

```sql
GRANT DBA TO admin1;
```

![pic13](E:\orace作业\test6\pic13.png)

##### 创建普通用户

```
CREATE USER user1 IDENTIFIED BY 123;
```

![pic14](E:\orace作业\test6\pic14.png)

##### 为普通用户赋予权限

```
GRANT SELECT, UPDATE ON product TO user1;
GRANT SELECT, UPDATE ON customer TO user1;
GRANT SELECT, UPDATE ON cart TO user1;
```

![pic15](E:\orace作业\test6\pic15.png)

#### PL/SQL设计

```sql
CREATE OR REPLACE PACKAGE ecommerce_pkg AS

   /* 定义变量 */
   /* ... */

   /* 定义存储过程 */
   PROCEDURE add_to_cart (
      p_customer_id IN NUMBER,
      p_product_id IN NUMBER,
      p_quantity IN NUMBER);

   PROCEDURE remove_from_cart (
      p_customer_id IN NUMBER,
      p_product_id IN NUMBER);

   PROCEDURE create_order (
      p_customer_id IN NUMBER);

   /* 定义函数 */
   FUNCTION get_customer_info (
      p_customer_id IN NUMBER)
      RETURN customer%ROWTYPE;

   FUNCTION get_product_info (
      p_product_id IN NUMBER)
      RETURN product%ROWTYPE;

   FUNCTION get_cart_items (
      p_customer_id IN NUMBER)
      RETURN SYS_REFCURSOR;

END ecommerce_pkg;
/
```

![pic16](E:\orace作业\test6\pic16.png)

![pic17](E:\orace作业\test6\pic17.png)

  在这个程序包中，定义了一些公共变量，并且定义了一些存储过程和函数来实现具体的业务逻辑。下面是一些例子，用来说明如何编写这些存储过程和函数的具体内容。

 1.添加商品到购物车

```
PROCEDURE add_to_cart (
   p_customer_id IN NUMBER,
   p_product_id IN NUMBER,
   p_quantity IN NUMBER)
IS
   v_stock NUMBER(10);
BEGIN
   -- 获取库存
   SELECT stock INTO v_stock FROM product WHERE id = p_product_id;

   -- 检查库存是否充足
   IF v_stock < p_quantity THEN
      RAISE_APPLICATION_ERROR(-20001, '库存不足');
   END IF;

   -- 检查购物车中是否已经有该商品
   SELECT COUNT(*) INTO v_count FROM cart WHERE customer_id = p_customer_id AND product_id = p_product_id;
   IF v_count > 0 THEN
      UPDATE cart SET quantity = quantity + p_quantity WHERE customer_id = p_customer_id AND product_id = p_product_id;
   ELSE
      INSERT INTO cart (customer_id, product_id, quantity) VALUES (p_customer_id, p_product_id, p_quantity);
   END IF;

   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
END add_to_cart;
```

  这个存储过程用来将某个商品添加到购物车中。在这个过程中，先获取该商品的库存，检查库存是否充足。如果库存不足，则抛出异常。如果购物车中已经有该商品，则将其数量加上新添加的数量；否则，插入一条新的记录。

2.从购物车中删除商品

```
PROCEDURE remove_from_cart (
   p_customer_id IN NUMBER,
   p_product_id IN NUMBER)
IS
BEGIN
   DELETE FROM cart WHERE customer_id = p_customer_id AND product_id = p_product_id;

   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
END remove_from_cart;
```

  这个存储过程用来从购物车中删除某个商品。

  3.创建订单

```
PROCEDURE create_order (
   p_customer_id IN NUMBER)
IS
   v_total_price NUMBER(10,2);
   v_order_status VARCHAR2(20);
   v_cart_items SYS_REFCURSOR;
   v_cart_record cart%ROWTYPE;
BEGIN
   -- 将购物车中所有商品的数量加起来，得到总价
   SELECT SUM(p.price * c.quantity) INTO v_total_price
   FROM cart c
   INNER JOIN product p ON c.product_id = p.id
   WHERE c.customer_id = p_customer_id;

   -- 检查是否有足够的库存
   OPEN v_cart_items FOR
      SELECT *
      FROM cart
      WHERE customer_id = p_customer_id;
   LOOP
      FETCH v_cart_items INTO v_cart_record;
      EXIT WHEN v_cart_items%NOTFOUND;
      SELECT stock INTO v_stock FROM product WHERE id = v_cart_record.product_id;
      IF v_stock < v_cart_record.quantity THEN
         CLOSE v_cart_items;
         RAISE_APPLICATION_ERROR(-20001, '库存不足');
      END IF;
   END LOOP;
   CLOSE v_cart_items;

   -- 创建订单
   v_order_status := 'Unpaid';
   INSERT INTO "ORDER" (customer_id, total, status) VALUES (p_customer_id, v_total_price, v_order_status);

   -- 将购物车中所有商品的数量从库存中扣除
   OPEN v_cart_items FOR
      SELECT *
      FROM cart
      WHERE customer_id = p_customer_id;
   LOOP
      FETCH v_cart_items INTO v_cart_record;
      EXIT WHEN v_cart_items%NOTFOUND;
      UPDATE product SET stock = stock - v_cart_record.quantity WHERE id = v_cart_record.product_id;
   END LOOP;
   CLOSE v_cart_items;

   -- 清空购物车
   DELETE FROM cart WHERE customer_id = p_customer_id;

   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
END create_order;
```

  这个存储过程用来创建一个订单。在这个过程中，将该顾客购物车中所有商品的数量相加，得到订单的总价和订单状态，并将这些数据插入到订单表中。然后，从库存中扣除购物车中所有商品的数量，并清空购物车。

  4.获取客户信息

```
FUNCTION get_customer_info (
   p_customer_id IN NUMBER)
   RETURN customer%ROWTYPE
IS
   v_customer customer%ROWTYPE;
BEGIN
   SELECT * INTO v_customer FROM customer WHERE id = p_customer_id;

   RETURN v_customer;
EXCEPTION
   WHEN NO_DATA_FOUND THEN
      RETURN NULL;
END get_customer_info;
```

   这个函数用来获取某个顾客的详细信息。如果找不到符合条件的记录，则返回NULL。

   5.获取商品信息

```
FUNCTION get_product_info (
   p_product_id IN NUMBER)
   RETURN product%ROWTYPE
IS
   v_product product%ROWTYPE;
BEGIN
   SELECT * INTO v_product FROM product WHERE id = p_product_id;

   RETURN v_product;
EXCEPTION
   WHEN NO_DATA_FOUND THEN
      RETURN NULL;
END get_product_info;
```

​    这个函数用来获取某个商品的详细信息。如果找不到符合条件的记录，则返回NULL。

​    6.获取购物车中的商品信息

```
FUNCTION get_cart_items (
   p_customer_id IN NUMBER)
   RETURN SYS_REFCURSOR
IS
   v_cart_items SYS_REFCURSOR;
BEGIN
   OPEN v_cart_items FOR
      SELECT c.id, p.name, p.price, c.quantity
      FROM cart c
      INNER JOIN product p ON c.product_id = p.id
      WHERE c.customer_id = p_customer_id;

   RETURN v_cart_items;
END get_cart_items;
```

   这个函数用来获取某个顾客的购物车中的所有商品信息。它返回一个游标，可以在客户端使用FETCH语句获取结果集。

##### 1.全量备份

```
expdp username/password@host:port/sid full=Y directory=backup_dir dumpfile=full_backup.dmp logfile=full_backup.log
```

  在上述命令中，需要指定用户名和密码、数据库连接信息、备份路径、备份文件名和日志文件名。执行该命令后，将会在指定目录下生成一个名为full_backup.dmp的备份文件和一个名为full_backup.log的日志文件。

##### 2.增量备份

```
expdp username/password@host:port/sid include=tablespace:'ts1' directory=backup_dir dumpfile=incremental_backup_%u.dmp logfile=incremental_backup.log content=metadata_only
```

  在上述命令中，include参数用来指定需要备份的表空间，directory参数用来指定备份路径，dumpfile参数用来指定备份文件名，%u表示增量备份序列号，logfile参数用来指定日志文件名，content参数指定备份内容为表结构和元数据。

##### 3.日志备份。

```
expdp username/password@host:port/sid dumpfile=log_backup_%U.dmp logfile=log_backup.log directory=backup_dir content=logs
```

   在上述命令中，dumpfile参数用来指定备份文件名，%U表示日志备份序列号，logfile参数用来指定日志文件名，directory参数用来指定备份路径，content参数指定备份内容为日志文件。

##### 4.数据恢复

```
impdp username/password@host:port/sid directory=backup_dir dumpfile=full_backup.dmp logfile=full_restore.log full=Y
```

   在上述命令中，需要指定用户名和密码、数据库连接信息、备份路径、备份文件名和日志文件名。执行该命令后，将会将备份文件full_backup.dmp中的数据导入到数据库中，完成数据恢复。

### 总结

​    在本次实验中，根据题目要求，我进行了用户创建、数据库连接、创建表空间、创建相关表，创建了商品信息表（PRODECT）订单信息表 (OWN_ORDER)，客户信息表(CUSTOMER)，购物车信息表 (CART)，销售记录表(PAYMENT)，之后使用PL/SQL编写脚本对数据表进行快速的填充，然后进行了权限的设计和用户的分配，两个用户有着对不同表的管理权限。最后进行了存储过程和函数的设计，创建了PA程序包，可以实现添加商品到购物车，从购物车中删除商品，创建订单，获取客户信息，获取商品信息，获取购物车中的商品信息等功能。整个数据库的设计不是很完善，表之间的关联性较弱，业务逻辑较少，会在将来的学习中对数据库进行进一步的完善，该实验加深了我对课程的理解和对oracle的应用。
